#Link: https://www.hackerrank.com/challenges/iterables-and-iterators

from itertools import combinations

l = input()
s = input().split(" ")
k = int(input())
count = 0

items = list(combinations(s, k))

for item in items:
    if('a' in item):
        count +=1

print(count/len(items))
