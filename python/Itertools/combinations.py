#Link: https://www.hackerrank.com/challenges/itertools-combinations
from itertools import combinations

s, n = input().split() 
for i in range(1, int(n)+1):
    items = list(combinations(sorted(list(s)), i))
    for item in items:
        print("".join(item))
