#link: https://www.hackerrank.com/challenges/itertools-product

from itertools import product 

def process_input(inp):
    inp = list(map(int, inp.split(" ")))
    return inp


if __name__ == '__main__':
    first = process_input(input())
    scnd = process_input(input())
    result = list(product(first, scnd))
    for i in result:
        print (i, end=" ")
