#Link: https://www.hackerrank.com/challenges/itertools-permutations
from itertools import permutations

s, n = input().split()    
items = permutations(sorted(list(s)), int(n))
for item in items:
    print("".join(item))
