#Link: https://www.hackerrank.com/challenges/maximize-it
from itertools import product

k, m = [int(x) for x in input().split()]
s = []

for i in range(k):
    tmp = list(map(int,(input().split())))
    s.append(tmp[1:])
    
groups = list(product(*s))

def get_sum(group):
    return sum(x**2 for x in group) % m

print(max(list(map(get_sum, groups))))

