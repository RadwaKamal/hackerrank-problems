#Link: https://www.hackerrank.com/challenges/itertools-combinations-with-replacement
from itertools import combinations_with_replacement

s, k = input().split()
items = list(combinations_with_replacement(sorted(list(s)), int(k)))

for item in items:
    print("".join(item))

