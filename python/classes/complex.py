#Link: https://www.hackerrank.com/challenges/class-1-dealing-with-complex-numbers

class ComplexNum(object):
    def __init__(self, real=0, img=0):
        self.real = real
        self.img = img
        
        
    def __add__(self, other):
        newreal = self.real + other.real
        newimg = self.img + other.img
        return ComplexNum(newreal, newimg)
    
    def __sub__(self, other):
        newreal = self.real - other.real
        newimg = self.img - other.img
        return ComplexNum(newreal, newimg)
    
    def __mul__(self, other):
        newreal = (self.real * other.real) - (self.img * other.img)
        newimg = (self.real * other.img) + (self.img * other.real)
        return ComplexNum(newreal, newimg)
    
    def __truediv__(self, other):
        den = other.real**2 + other.img**2
        newreal = round(((self.real * other.real) - (self.img * -other.img))/den, 2)
        newimg = round(((self.real * -other.img) + (self.img * other.real))/den, 2)
        return ComplexNum(newreal, newimg)

    def __abs__(self):
        real = self.real**2 + self.img**2
        newreal = round(real**0.5, 2)
        return ComplexNum(newreal)
    
    def __str__(self):
        if(self.img<0):
            return str('%.2f' % self.real) + str('%.2f' %self.img) + "i"
        else:
            return str('%.2f' %self.real) + "+" + str('%.2f' %self.img) + "i"

        
r1, i1 = [float(x) for x in input().split()]
r2, i2 = [float(x) for x in input().split()]
c1 = ComplexNum(r1, i1)
c2 = ComplexNum(r2, i2)
for x in [c1+c2, c1-c2, c1*c2, c1/c2, abs(c1), abs(c2)]:
    print(x)

